package com.example.exercices;

import com.example.exercices.entities.Produit;
import com.example.exercices.services.ProduitService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet(name = "ProduitsServlet", value = "/listeproduits")
public class ProduitsServlet extends HttpServlet {

    private List<Produit> produits;
    private ProduitService produitService;

    public void init() {
        produitService = new ProduitService();
        produits = produitService.getAllProduits();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("id") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            Produit produit = null;
            try {
                produit = produitService.getProduit(id);
            } catch (Exception e) {

            }
            if (produit != null) {
                request.setAttribute("produit", produit);
                request.getRequestDispatcher("detailproduit.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("erreur.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("produits", produits);
            request.getRequestDispatcher("produits.jsp").forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!request.getParameter("marque").isBlank() && !request.getParameter("reference").isBlank() &&
                !request.getParameter("dateAchat").isBlank() && !request.getParameter("prix").isBlank() &&
                !request.getParameter("stock").isBlank()) {
            String marque = request.getParameter("marque");
            String reference = request.getParameter("reference");
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-mm-dd").parse(request.getParameter("dateAchat"));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            double prix = Double.parseDouble(request.getParameter("prix"));
            int stock = Integer.parseInt(request.getParameter("stock"));
            Produit produit = new Produit(marque, reference, date, prix, stock);
            if (produitService.createProduit(produit)) {
                produits = produitService.getAllProduits();
                request.setAttribute("produits", produits);
                request.getRequestDispatcher("produits.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("erreur.jsp").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("erreur.jsp").forward(request, response);
        }
    }
}

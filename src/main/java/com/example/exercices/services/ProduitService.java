package com.example.exercices.services;

import com.example.exercices.entities.Produit;
import com.example.exercices.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.List;

public class ProduitService implements IDAO<Produit> {

    private StandardServiceRegistry registre;
    private SessionFactory sessionFactory;
    private static Session session;

    public ProduitService() {
        registre = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        session = sessionFactory.openSession();
    }

    @Override
    public boolean create(Produit o) {
        session.save(o);
        return true;
    }

    @Override
    public boolean update(Produit o) {
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(Produit o) {
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Produit findById(int id) {
        Produit produit = null;
        session.beginTransaction();
        produit = (Produit)session.get(Produit.class, id);
        session.getTransaction().commit();
        return produit;
    }

    @Override
    public List<Produit> findAll() {
        session.beginTransaction();
        Query<Produit> produitQuery= session.createQuery("from Produit");
        session.getTransaction().commit();
        return produitQuery.list();
    }

    // Exercice 1

    public List<Produit> getAllProduits() {
        List<Produit> produits = session.createQuery("from Produit").list();
        return produits;
    }

    // Exercice 2

    public Produit getProduit(int id) {
        return session.get(Produit.class, id);
    }

    public boolean createProduit(Produit p) {
        session.save(p);
        return true;
    }
}

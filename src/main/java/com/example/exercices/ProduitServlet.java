package com.example.exercices;

import com.example.exercices.entities.Produit;
import com.example.exercices.services.ProduitService;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(name = "produitServlet", value = "/produits")
public class ProduitServlet extends HttpServlet {

    private List<Produit> produits = new ArrayList<>();
    private ProduitService produitService;

    public void init() {
        produitService = new ProduitService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        produits = produitService.getAllProduits();
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("<html><body>");

        if (request.getParameter("id") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            if (produitService.getProduit(id) != null) {
                Produit produit = produitService.getProduit(id);
                writer.println("<div>" + produit + "</div>");
            } else {
                writer.println("<div>Aucun produit ne correspond à cet id.</div>");
            }
        } else {
            writer.println("<table style=\"border: 1px solid black; border-collapse: collapse;\">" +
                    "<tr>" +
                    "<th style=\"border: 1px solid black; border-collapse: collapse; background: blue; color: white; padding: 15px;\">Id</th>" +
                    "<th style=\"border: 1px solid black; border-collapse: collapse; background: blue; color: white; padding: 15px;\">Marque</th>" +
                    "<th style=\"border: 1px solid black; border-collapse: collapse; background: blue; color: white; padding: 15px;\">Référence</th>" +
                    "<th style=\"border: 1px solid black; border-collapse: collapse; background: blue; color: white; padding: 15px;\">Date d'achat</th>" +
                    "<th style=\"border: 1px solid black; border-collapse: collapse; background: blue; color: white; padding: 15px;\">Prix</th>" +
                    "<th style=\"border: 1px solid black; border-collapse: collapse; background: blue; color: white; padding: 15px;\">Stock</th>" +
                    "</tr>");
            for (Produit p : produits) {
                writer.println("<tr>" +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 15px;\">" + p.getId() + "</td>" +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 15px;\">" + p.getMarque() + "</td>" +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 15px;\">" + p.getReference() + "</td>" +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 15px;\">" + p.getDateAchat() + "</td>" +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 15px;\">" + p.getPrix() + "</td>" +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 15px;\">" + p.getStock() + "</td>" +
                        "</tr>");
            }
            writer.println("</table>");
        }

        writer.println("</body></html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("<html><body>");

        Produit newProduit = new Produit();

        if (request.getParameter("marque") != null && request.getParameter("reference") != null &&
                request.getParameter("dateAchat") != null && request.getParameter("prix") != null &&
                request.getParameter("stock") != null) {
            newProduit.setMarque(request.getParameter("marque"));
            newProduit.setReference(request.getParameter("reference"));
            Date date = null;
            try {
                date = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dateAchat"));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            newProduit.setDateAchat(date);
            newProduit.setPrix(Double.valueOf(request.getParameter("prix")));
            newProduit.setStock(Integer.parseInt(request.getParameter("stock")));

            if (produitService.createProduit(newProduit)) {
                writer.println("<div>Produit créé : " + newProduit + "</div>");
            } else {
                writer.println("<div>Erreur pendant la création d'un produit.</div>");
            }
        } else {
            writer.println("<div>Erreur dans les paramètres.</div>");
        }

        writer.println("</body></html>");
    }
}

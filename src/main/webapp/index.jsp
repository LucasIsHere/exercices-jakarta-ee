<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Page d'accueil" %>
</h1>
<br/>
<ul>
    <li><a href="hello-servlet">Hello Servlet</a></li>
    <li><a href="produits">Produits</a></li>
    <li><a href="listeproduits">Liste produits JSP</a></li>
</ul>
</body>
</html>
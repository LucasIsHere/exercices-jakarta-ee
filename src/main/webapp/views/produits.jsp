<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrateur
  Date: 07/12/2022
  Time: 11:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liste des produits</title>
</head>
<body>
<h2>Liste des produits :</h2>
<br>
<ul>
    <c:forEach items="${produits}" var="produit">
        <li>
            Id produit : ${produit.getId()},
            marque : ${produit.getMarque()},
            référence : ${produit.getReference()},
            <a href="listeproduits?id=${produit.getId()}">Voir le détail</a>
        </li>
    </c:forEach>
</ul>
<hr>
<h2>Ajouter un produit :</h2>
<br>
<div>
    <form method="post" action="">
        <label for="marque">Marque : </label>
        <input type="text" name="marque" id="marque">
        <label for="reference">Référence : </label>
        <input type="text" name="reference" id="reference">
        <label for="dateAchat">Date d'achat : </label>
        <input type="date" name="dateAchat" id="dateAchat">
        <label for="prix">Prix : </label>
        <input type="text" name="prix" id="prix">
        <label for="stock">Stock : </label>
        <input type="number" name="stock" id="stock">
        <input type="submit">
    </form>
</div>
</body>
</html>
